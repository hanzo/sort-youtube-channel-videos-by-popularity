// ==UserScript==
// @name        Sort by popularity
// @description Sets Youtube channel videos sorted by popularity if not set
// @icon        https://www.youtube.com/favicon.ico
// @match       https://www.youtube.com/*
// @run-at      document-start
// @updateURL  https://framagit.org/hanzo/sort-youtube-channel-videos-by-popularity/raw/master/yt-channelvids-sortbypop.user.js
// ==/UserScript==

function sortByPopularity() {
    var url = new URL(window.location);
    var params = new URLSearchParams(url.search);
    console.debug(params.toString());
    if ( !params.has("sort") || params.toString() == "" ) {
        window.location = url.toString( url.searchParams.set('sort', 'p') );
    }
}

// sort on matching XHR 
(function(open) {
    XMLHttpRequest.prototype.open = function() {
        this.addEventListener("readystatechange", function() {
            if (this.readyState == 4) {
                if ( this.responseURL.match(/videos\?pbj=1$/) != undefined ) {
                    sortByPopularity();
                }
            }
        }, false);
        open.apply(this, arguments);
    };
})(XMLHttpRequest.prototype.open);